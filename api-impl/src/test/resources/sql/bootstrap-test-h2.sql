-- Table T_MEMBER
CREATE CACHED TABLE PUBLIC.T_MEMBER(
	ID VARCHAR(32) NOT NULL,
	FIRST_NAME VARCHAR(35) NOT NULL,
	LAST_NAME VARCHAR(35) NOT NULL,
	BIRTHDAY TIMESTAMP NOT NULL,
	PHONE_NUMBER VARCHAR(15) NOT NULL,
	EMAIL VARCHAR(50) NOT NULL,
	MARK_GOALS INT,
	MARK_SUPPORTS INT,
	MARK_CHALLENGES INT,
	CREATION_DATE TIMESTAMP NOT NULL,
    MODIFIED_DATE TIMESTAMP NOT NULL,
    CREATED_BY VARCHAR(32) NOT NULL,
    MODIFIED_BY VARCHAR(32) NOT NULL,
);

-- Table T_MEMBER
CREATE CACHED TABLE PUBLIC.T_SYSTEM__MEMBER(
	ID VARCHAR(32) NOT NULL,
	FIRST_NAME VARCHAR(35) NOT NULL,
	LAST_NAME VARCHAR(35) NOT NULL,
	BIRTHDAY TIMESTAMP NOT NULL,
	PHONE_NUMBER VARCHAR(15) NOT NULL,
	EMAIL VARCHAR(50) NOT NULL,
	CREATION_DATE TIMESTAMP NOT NULL,
    MODIFIED_DATE TIMESTAMP NOT NULL,
    CREATED_BY VARCHAR(32) NOT NULL,
    MODIFIED_BY VARCHAR(32) NOT NULL,
);

-- Table T_ACCOUNT
CREATE CACHED TABLE PUBLIC.T_ACCOUNT(
	ID VARCHAR(32) NOT NULL,
	USERNAME VARCHAR(50) NOT NULL,
	PASSWORD VARCHAR(50) NOT NULL,
	MEMBER_ID VARCHAR(32) NOT NULL,
	ACCOUNT_TYPE VARCHAR(5) NOT NULL,
	CREATION_DATE TIMESTAMP NOT NULL,
    MODIFIED_DATE TIMESTAMP NOT NULL,
    CREATED_BY VARCHAR(32) NOT NULL,
    MODIFIED_BY VARCHAR(32) NOT NULL,
);

-- Table ACCOUNT_STATUS
CREATE CACHED TABLE PUBLIC.T_ACCOUNT_STATUS(
	ID VARCHAR(32) NOT NULL,
	ACCOUNT VARCHAR(32) NOT NULL,
	STATUS VARCHAR(5) NOT NULL,
	VALID_FROM TIMESTAMP NOT NULL,
    VALID_TO TIMESTAMP,
	CREATION_DATE TIMESTAMP NOT NULL,
    MODIFIED_DATE TIMESTAMP NOT NULL,
    CREATED_BY VARCHAR(32) NOT NULL,
    MODIFIED_BY VARCHAR(32) NOT NULL,
    FOREIGN KEY (ACCOUNT) REFERENCES T_ACCOUNT(ID),
);

-- Table FRIEND_REQUEST
CREATE CACHED TABLE PUBLIC.T_FRIEND_REQUEST(
	ID VARCHAR(32) NOT NULL,
	SENDER VARCHAR(32) NOT NULL,
	RECEIVER VARCHAR(32) NOT NULL,
	STATUS VARCHAR(5) NOT NULL,
	CREATION_DATE TIMESTAMP NOT NULL,
    MODIFIED_DATE TIMESTAMP NOT NULL,
    CREATED_BY VARCHAR(32) NOT NULL,
    MODIFIED_BY VARCHAR(32) NOT NULL,
    FOREIGN KEY (SENDER) REFERENCES T_MEMBER(ID),
    FOREIGN KEY (RECEIVER) REFERENCES T_MEMBER(ID),
);

-- Table FRIEND_LIST
CREATE CACHED TABLE PUBLIC.T_FRIEND_LIST(
	ID VARCHAR(32) NOT NULL,
	OWNER VARCHAR(32) NOT NULL,
	FOREIGN KEY (OWNER) REFERENCES T_MEMBER(ID),
);