/**
 * 
 */
package com.huynhgia.tutor.dao.impl;

import com.huynhgia.tutor.HibernateDaoTemplate;
import com.huynhgia.tutor.dao.RequestTutorDAO;
import com.huynhgia.tutor.dto.RequestTutor;

/**
 * @author Anh Tuan Truong
 *
 */
public class RequestTutorDAOImpl extends HibernateDaoTemplate<RequestTutor> implements RequestTutorDAO {

	@SuppressWarnings("rawtypes")
	public RequestTutorDAOImpl(Class classType) {
		super(classType);
	}

}
