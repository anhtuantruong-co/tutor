/**
 * 
 */
package com.huynhgia.tutor.dao.impl;

import java.security.Principal;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.huynhgia.tutor.HibernateDaoTemplate;
import com.huynhgia.tutor.dao.AccountDAO;
import com.huynhgia.tutor.dto.Account;
import com.huynhgia.tutor.exception.FatalException;
import com.huynhgia.tutor.utils.TutorUtils;

/**
 * @author Anh Tuan Truong
 *
 */
public class AccountDAOImpl extends HibernateDaoTemplate<Account> implements AccountDAO {

	@SuppressWarnings("rawtypes")
	public AccountDAOImpl(Class classType) {
		super(classType);
	}

	@Override
	public Account findByUsername(String username) throws FatalException {
		DetachedCriteria cri = DetachedCriteria.forClass(Account.class);
		cri.add(Restrictions.eq("username", username));
		return findFirstByCriteria(cri);
	}

	@Override
	public Principal authenticate(String username, String password) throws FatalException {
		Account user = findByUsername(username);
		if (user.getPassword() != null && user.getPassword().equals(password)
				&& TutorUtils.isActive(user))
			return user;

		return null;
	}

	@Override
	public void updateAccount(Account account) throws FatalException {
		super.update(account);
	}

}
