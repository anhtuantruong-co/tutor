/**
 * 
 */
package com.huynhgia.tutor.dao.impl;

import com.huynhgia.tutor.HibernateDaoTemplate;
import com.huynhgia.tutor.dao.RequestParentDAO;
import com.huynhgia.tutor.dto.RequestParent;

/**
 * @author Anh Tuan Truong
 *
 */
public class RequestParentDAOImpl extends HibernateDaoTemplate<RequestParent> implements RequestParentDAO {

	@SuppressWarnings("rawtypes")
	public RequestParentDAOImpl(Class classType) {
		super(classType);
	}

}
