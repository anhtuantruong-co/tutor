/**
 * 
 */
package com.huynhgia.tutor.dao.impl;

import com.huynhgia.tutor.HibernateDaoTemplate;
import com.huynhgia.tutor.dao.TutorDAO;
import com.huynhgia.tutor.dto.Tutor;

/**
 * @author Anh Tuan Truong
 *
 */
public class TutorDAOImpl extends HibernateDaoTemplate<Tutor> implements TutorDAO {

	@SuppressWarnings("rawtypes")
	public TutorDAOImpl(Class classType) {
		super(classType);
	}

}
