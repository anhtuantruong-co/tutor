/**
 * 
 */
package com.huynhgia.tutor.dao.impl;

import com.huynhgia.tutor.HibernateDaoTemplate;
import com.huynhgia.tutor.dao.ClassDAO;
import com.huynhgia.tutor.dto.ClassFile;

/**
 * @author Anh Tuan Truong
 *
 */
public class ClassDAOImpl extends HibernateDaoTemplate<ClassFile> implements ClassDAO {

	@SuppressWarnings("rawtypes")
	public ClassDAOImpl(Class classType) {
		super(classType);
	}

}
