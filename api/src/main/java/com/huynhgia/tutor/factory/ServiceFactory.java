/**
 * 
 */
package com.huynhgia.tutor.factory;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;

import com.huynhgia.tutor.exception.FatalException;

/**
 * @author Anh Tuan Truong
 *
 */
public class ServiceFactory {
	
	public static Object getService(String service) throws FatalException{
		try {
            BeanFactoryLocator bfl = new TutorSingletonBeanFactoryLocator();
            BeanFactoryReference bf = bfl.useBeanFactory(TutorSingletonBeanFactoryLocator.DEFAULT_GOALLIFE_FACTORY_KEY);
            return bf.getFactory().getBean(service);
        } catch (BeansException e) {
            /*logger.error("could not get bean " + service, e);*/
            throw new FatalException("Requested service " + service + " not found", e);
        }
	}
	
	public static <T> T getService(Class<T> clazz) throws FatalException {
        try {
            BeanFactoryLocator bfl = new TutorSingletonBeanFactoryLocator();
            BeanFactoryReference bf = bfl.useBeanFactory(TutorSingletonBeanFactoryLocator.DEFAULT_GOALLIFE_FACTORY_KEY);
            return bf.getFactory().getBean(clazz);
        } catch (BeansException e) {
            /*logger.error("could not get bean " + clazz, e);*/
            throw new FatalException("Requested service " + clazz + " not found", e);
        }
    }

}
