package com.huynhgia.tutor.factory;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.access.ContextBeanFactoryReference;

/**
 * BeanFactoryLocator meant to be initialized during context startup.
 * This is used by the ServiceFactory to locate all the beans.
 * <p/>
 * Since we don't know in advance how the context will be started (it could be via
 * a ServletContext listener if we are in standalone mode, or via a spring deployer
 * in jboss in EJB mode, or any other mean), we need this to hide the details of
 * context location from the application.
 * <p/>
 * The default beanFactoryKey is {@link #DEFAULT_GOALLIFE_FACTORY_KEY}. The rest of the CMS
 * expects this. You may override this value by setting the property via {@link #setBeanFactoryName(String)},
 * when this locator is registered in the context - but YMMV.
 * 
 * @author Anh Tuan Truong
 *
 */
public class TutorSingletonBeanFactoryLocator implements ApplicationContextAware, BeanFactoryLocator {
	
	private static final Map beanReferences = new HashMap();

	public static final String DEFAULT_GOALLIFE_FACTORY_KEY = "com.brother.goallife";
	
	private String beanFactoryName = DEFAULT_GOALLIFE_FACTORY_KEY;

	@Override
	public BeanFactoryReference useBeanFactory(String factoryKey)
			throws BeansException {
		synchronized (beanReferences) {
            BeanFactoryReference br = (BeanFactoryReference)beanReferences.get(factoryKey);
            if (br == null) {
                throw new BeanDefinitionStoreException("no beanfactory <" + factoryKey +
                                                       "> is defined - make sure you add a " + this.getClass() +
                                                       " to your application context to initialise the locator singleton");
            }
            return br;
        }
	}

	@Override
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		synchronized (beanReferences) {
            /*logger.info("recording context <" + context + "> at <" + beanFactoryName + ">");*/
            beanReferences.put(beanFactoryName, new ContextBeanFactoryReference(context));
        }
		
	}
	
	public void setBeanFactoryName(String beanFactoryName) {
        this.beanFactoryName = beanFactoryName;
    }

}
