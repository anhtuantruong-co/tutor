/**
 * 
 */
package com.huynhgia.tutor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author anhtuan_truong
 *
 */
public interface Auditable {
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date", updatable = false, insertable = true)
	public Date getCreationDate();
	
	public void setCreationDate(Date creationDate);
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date", updatable = false, insertable = true)
	public Date getModifiedDate();
	
	public void setModifiedDate(Date modifiedDate);

}
