/**
 * 
 */
package com.huynhgia.tutor.exception;

/**
 * @author Anh Tuan Truong
 *
 */
public class FatalException extends TutorException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FatalException() {
	}
	
	public FatalException(String message) {
		super(message);
	}
	
	public FatalException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
