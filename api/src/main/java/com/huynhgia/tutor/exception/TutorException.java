/**
 * 
 */
package com.huynhgia.tutor.exception;

/**
 * @author Anh Tuan Truong
 *
 */
public class TutorException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TutorException() {
	}
	
	public TutorException(String message) {
		super(message);
	}
	
	public TutorException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
