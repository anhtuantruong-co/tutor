/**
 * 
 */
package com.huynhgia.tutor.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.huynhgia.tutor.AbstractTutorEntity;

/**
 * @author Anh Tuan Truong
 *
 */
@Entity
@Table(name = "T_SYSTEM_MEMBER")
public class SystemMember extends AbstractTutorEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6910299727101968071L;
	
	private String firstname;
	private String lastname;
	private Date birthday;
	private String phoneNumber;
	private String email;

	@Column(name = "FIRST_NAME", nullable = false, length = 35)
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Column(name = "LAST_NAME", nullable = false, length = 35)
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "BIRTHDATE", nullable = false)
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Column(name = "PHONE_NUMBER", nullable = false, length = 15)
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "EMAIL", nullable = false, length = 50)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
