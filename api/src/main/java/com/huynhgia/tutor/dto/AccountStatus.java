/**
 * 
 */
package com.huynhgia.tutor.dto;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.huynhgia.tutor.AbstractTutorEntity;

/**
 * @author Anh Tuan Truong
 *
 */
@Entity
@Table(name = "T_ACCOUNT_STATUS")
public class AccountStatus extends AbstractTutorEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4902399302124306372L;

}
