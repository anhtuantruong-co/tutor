package com.huynhgia.tutor.dto;

import java.security.Principal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.huynhgia.tutor.AbstractTutorEntity;


/**
 * 
 * @author anhtuan_truong
 * 
 */
@Entity
@Table(name = "T_ACCOUNT")
public class Account extends AbstractTutorEntity implements Principal {


	/**
	 * 
	 */
	private static final long serialVersionUID = 9138224457910803530L;
	
	
	private String username;
	private String password;
	private String accountType;
	private Set<AccountStatus> statuses;
	
	@Column(name="USERNAME", nullable = false, length = 50)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name="PASSWORD", nullable = false, length = 50)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name="ACCOUNT_TYPE", nullable = false, length = 5)
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	@OneToMany(mappedBy="account", fetch=FetchType.EAGER, orphanRemoval=true, cascade=CascadeType.ALL)
	public Set<AccountStatus> getStatuses() {
		return statuses;
	}
	
	public void setStatuses(Set<AccountStatus> statuses) {
		this.statuses = statuses;
	}

	@Transient
	public String getName() {
		return username;
	}
	
	

}
