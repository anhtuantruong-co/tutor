/**
 * 
 */
package com.huynhgia.tutor.dto;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.huynhgia.tutor.AbstractTutorEntity;

/**
 * @author Anh Tuan Truong
 *
 */
@Entity
@Table(name = "T_CLASS_FILE")
public class ClassFile extends AbstractTutorEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -221933361216076238L;

	private String serialNumber;
	private int grade;
	private String subject;
	private String address;
	private String schdule;
	private int salary;
	private String demand;

	private Set<ClassStatus> statuses;
	private Tutor tutor;

	@Column(name = "SERIAL_NUMBER", nullable = false, unique = true, length = 9)
	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Column(name = "GRADE", nullable = false, length = 5)
	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	@Column(name = "SUBJECT", nullable = false, length = 35)
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "ADDRESS", nullable = false, length = 100)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "SCHEDULE", nullable = false, length = 35)
	public String getSchdule() {
		return schdule;
	}

	public void setSchdule(String schdule) {
		this.schdule = schdule;
	}
	
	@Column(name = "SALARY", nullable = false, length = 5)
	public int getSalary() {
		return salary;
	}
	
	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Column(name = "DEMAND", length = 100)
	public String getDemand() {
		return demand;
	}

	public void setDemand(String demand) {
		this.demand = demand;
	}

	@OneToMany(mappedBy="class", fetch=FetchType.EAGER, orphanRemoval=true, cascade=CascadeType.ALL)
	public Set<ClassStatus> getStatuses() {
		return statuses;
	}

	public void setStatuses(Set<ClassStatus> statuses) {
		this.statuses = statuses;
	}

	@ManyToOne(targetEntity = Tutor.class)
	@JoinColumn(name = "tutor")
	public Tutor getTutor() {
		return tutor;
	}

	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

}
