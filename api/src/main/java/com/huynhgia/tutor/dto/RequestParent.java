/**
 * 
 */
package com.huynhgia.tutor.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.huynhgia.tutor.AbstractTutorEntity;

/**
 * @author Anh Tuan Truong
 *
 */
@Entity
@Table(name = "T_REQUEST_PARENT")
public class RequestParent extends AbstractTutorEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7791993004645791918L;

	// contact information
	private String city;
	private String section;
	private String name;
	private String cellPhone;
	private String homePhone;
	private String email;
	private String address;

	// class information
	private String school;
	private String schedule;
	private int salary;
	private String subject;

	// demand information
	private String demand;
	private String otherDemand;

	private String status;

	@Column(name = "CITY", nullable = false, length = 17)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "SECTION", length = 17)
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(name = "NAME", nullable = false, length = 35)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "PHONE_NUMBER", nullable = false, length = 12)
	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	@Column(name = "HOME_PHONE", length = 12)
	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	@Column(name = "EMAIL", length = 50)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "ADDRESS", nullable = false, length = 50)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "SCHOOL", nullable = false, length = 35)
	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	@Column(name = "SCHEDULE", nullable = false, length = 35)
	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	@Column(name = "SALARY", nullable = false, length = 5)
	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Column(name = "SUBJECT", nullable = false, length = 35)
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "DEMAND", length = 100)
	public String getDemand() {
		return demand;
	}

	public void setDemand(String demand) {
		this.demand = demand;
	}

	@Column(name = "OTHER_DEMAND", length = 1000)
	public String getOtherDemand() {
		return otherDemand;
	}

	public void setOtherDemand(String otherDemand) {
		this.otherDemand = otherDemand;
	}

	@Column(name="STATUS", nullable = false, length = 5)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
