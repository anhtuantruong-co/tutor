/**
 * 
 */
package com.huynhgia.tutor.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.huynhgia.tutor.AbstractTutorEntity;

/**
 * @author tuan
 *
 */
@Entity
@Table(name = "T_TUTOR")
public class Tutor extends AbstractTutorEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6267161783704174807L;

	private String firstname;
	private String lastname;
	private Date birthdate;
	private String birthCountry;
	private String address;
	private String phoneNumber;
	private String gender;
	private String email;
	
	private String level;
	private String university;
	private String major;
	private int yearGraduate;
	private String currentState;
	
	// TODO: picture of tutor
	
	private String status;

	@Column(name = "FIRST_NAME", nullable = false, length = 35)
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Column(name = "LAST_NAME", nullable = false, length = 35)
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "BIRTHDATE", nullable = false)
	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	@Column(name = "BIRTH_COUNTRY", nullable = false, length = 17)
	public String getBirthCountry() {
		return birthCountry;
	}
	
	public void setBirthCountry(String birthCountry) {
		this.birthCountry = birthCountry;
	}

	@Column(name = "ADDRESS", nullable = false, length = 100)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "PHONE_NUMBER", nullable = false, length = 12)
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Column(name = "GENDER", nullable = false, length = 5)
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Column(name = "EMAIL", length = 35)
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "LEVEL", nullable = false, length = 5)
	public String getLevel() {
		return level;
	}
	
	public void setLevel(String level) {
		this.level = level;
	}
	
	@Column(name = "UNIVERSITY", nullable = false, length = 5)
	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	@Column(name = "MAJOR", nullable = false, length = 17)
	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	@Column(name = "YEAR_GRADUATE", nullable = false, length = 5)
	public int getYearGraduate() {
		return yearGraduate;
	}

	public void setYearGraduate(int yearGraduate) {
		this.yearGraduate = yearGraduate;
	}

	@Column(name = "CURRENT_STATE", nullable = false, length = 5)
	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	@Column(name="STATUS", nullable = false, length = 5)
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

}
