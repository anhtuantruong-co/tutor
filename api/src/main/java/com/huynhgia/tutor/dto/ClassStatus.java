/**
 * 
 */
package com.huynhgia.tutor.dto;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.huynhgia.tutor.AbstractTutorEntity;

/**
 * @author Anh Tuan Truong
 *
 */
@Entity
@Table(name = "T_CLASS_STAUS")
public class ClassStatus extends AbstractTutorEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 418776327387761640L;

}
