/**
 * 
 */
package com.huynhgia.tutor.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.huynhgia.tutor.AbstractTutorEntity;

/**
 * @author Anh Tuan Truong
 *
 */
@Entity
@Table(name = "T_REQUEST_TUTOR")
public class RequestTutor extends AbstractTutorEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7791993004645791918L;
	private String city;
	private String section;
	

	private String schedule;
	private int salary;
	private List<String> subjects;

	private String status;

	@Column(name = "CITY", nullable = false, length = 17)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "SECTION", length = 17)
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(name = "SCHEDULE", nullable = false, length = 35)
	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	@Column(name = "SALARY", nullable = false, length = 5)
	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Transient
	public List<String> getSubjects() {
		return subjects;
	}
	
	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}
	

	@Column(name="STATUS", nullable = false, length = 5)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
