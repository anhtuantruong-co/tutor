package com.huynhgia.tutor.type;

import com.huynhgia.tutor.exception.TutorException;

/**
 * 
 * @author Anh Tuan Truong
 *
 */
public enum AccountType  {
	
	ORDIN("ORDIN"),
	SYSTE("SYSTE"),
	ADMIN("ADMIN");

	
	private String code;
	
	private AccountType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public AccountType getTypeFromCode(String code) throws TutorException{
		for (AccountType item : AccountType.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		
		throw new TutorException("This enum type is not supprted");
	}
}
