package com.huynhgia.tutor.type;

import com.huynhgia.tutor.exception.TutorException;

public enum RequestParentType {
	
	PENDING("PENDI"),
	CLOSE("CLOSE");
	
	private String code;
	
	private RequestParentType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public RequestParentType getTypeFromCode(String code) throws TutorException{
		for (RequestParentType item : RequestParentType.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		
		throw new TutorException("This enum type is not supprted");
	}

}
