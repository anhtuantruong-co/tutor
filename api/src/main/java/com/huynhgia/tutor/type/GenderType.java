/**
 * 
 */
package com.huynhgia.tutor.type;

import com.huynhgia.tutor.exception.TutorException;

/**
 * @author Anh Tuan Truong
 *
 */
public enum GenderType {
	
	MALE("MALE_"),
	FEMALE("FEMAL");

	
	private String code;
	
	private GenderType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public GenderType getTypeFromCode(String code) throws TutorException{
		for (GenderType item : GenderType.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		
		throw new TutorException("This enum type is not supprted");
	}

}
