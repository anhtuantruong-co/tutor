package com.huynhgia.tutor.type;

import com.huynhgia.tutor.exception.TutorException;

/**
 * 
 * @author Anh Tuan Truong
 *
 */
public enum TutorStateType  {
	
	STUDENT("STUDE"),
	TEACHER("TEACH"),
	GRADUATED("GRADU");

	
	private String code;
	
	private TutorStateType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public TutorStateType getTypeFromCode(String code) throws TutorException{
		for (TutorStateType item : TutorStateType.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		
		throw new TutorException("This enum type is not supprted");
	}
}
