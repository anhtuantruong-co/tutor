/**
 * 
 */
package com.huynhgia.tutor.type;

import com.huynhgia.tutor.exception.TutorException;

/**
 * @author Anh Tuan Truong
 *
 */
public enum TutorStatusType {
	
	PENDING("PENDI"),
	ACTIVE("ACTIV"),
	CLOSE("CLOSE");
	
	private String code;
	
	private TutorStatusType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public TutorStatusType getTypeFromCode(String code) throws TutorException{
		for (TutorStatusType item : TutorStatusType.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		
		throw new TutorException("This enum type is not supprted");
	}
}
