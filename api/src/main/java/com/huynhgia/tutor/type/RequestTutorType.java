package com.huynhgia.tutor.type;

import com.huynhgia.tutor.exception.TutorException;

public enum RequestTutorType {
	
	PENDING("PENDI"),
	CLOSE("CLOSE");
	
	private String code;
	
	private RequestTutorType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public RequestTutorType getTypeFromCode(String code) throws TutorException{
		for (RequestTutorType item : RequestTutorType.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		
		throw new TutorException("This enum type is not supprted");
	}

}
