package com.huynhgia.tutor.type;

import com.huynhgia.tutor.exception.TutorException;

/**
 * 
 * @author Anh Tuan Truong
 *
 */
public enum TutorLevelType  {
	
	STUDENT("STUDE"),
	BACLELOR("BACLE"),
	MASTER("MASTE"),
	PHD("PHD__");

	
	private String code;
	
	private TutorLevelType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	public TutorLevelType getTypeFromCode(String code) throws TutorException{
		for (TutorLevelType item : TutorLevelType.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		
		throw new TutorException("This enum type is not supprted");
	}
}
