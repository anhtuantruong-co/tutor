/**
 * 
 */
package com.huynhgia.tutor;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

/*
 * @author anhtuan_truong
 * 
 * Using this annotation for children not having exception
 * org.hibernate.AnnotationException: No identifier specified for entity.
 * 
 * MappedSuperClass must be used to inherit properties, associations, and
 * methods. Entity inheritance must be used when you have an entity, and several
 * sub-entities.
 * 
 * @link
 * http://stackoverflow.com/questions/9667703/jpa-implementing-model-hiearchy
 * -mappedsuperclass-vs-inheritence
 */
@MappedSuperclass
public class AbstractTutorEntity implements Serializable, Auditable,
		Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected int id;
	private Date creationDate;
	private Date modifiedDate;
	private String createdBy;
	private String modifiedBy;
	
	private int version;
	
	@Id
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	@Column(name="ID", length = 32)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE", insertable = true)
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE", insertable = true)
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	
	@Column(name = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name = "MODIFIED_BY")
	public String getModifiedBy() {
		return modifiedBy;
	}
	
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@Version
	@Column(name = "VERSION", nullable = false)
	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}
}
