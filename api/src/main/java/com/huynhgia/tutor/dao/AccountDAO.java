/**
 * 
 */
package com.huynhgia.tutor.dao;

import java.security.Principal;

import com.huynhgia.tutor.dto.Account;
import com.huynhgia.tutor.exception.FatalException;

/**
 * @author Anh Tuan Truong
 *
 */
public interface AccountDAO {
	
	public Account findByUsername(String username) throws FatalException;

	/**
	 * Login user
	 */
	public Principal authenticate(String username, String password) throws FatalException;
	
	
	public void updateAccount(Account account) throws FatalException;

}
