/**
 * 
 */
package com.huynhgia.tutor;

import java.io.Serializable;
import java.security.Principal;

/**
 * @author Anh Tuan Truong
 * TODO:
 *
 */
public class Context implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5732727953097281529L;

	/**
     * Key used to store current context into the HttpSession
     */
    public static final String SESSION_KEY = "_context";

    private Principal principal;
    
    public Context() {
		this.principal = null;
	}
    
    public Principal getPrincipal() {
		return principal;
	}
    
    public void setPrincipal(Principal principal) {
		this.principal = principal;
	}

}
